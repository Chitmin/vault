const path = require("path");
const BASE_PATH = path.join(__dirname, "src", "db");
require("dotenv").config();

module.exports = {
  production: {
    client: "mysql",
    connection: {
      host: process.env.MYSQL_HOST || "127.0.0.1",
      user: process.env.MYSQL_USER,
      password: process.env.MYSQL_PASSWORD,
      database: process.env.MYSQL_DATABASE || "vault"
    },
    migrations: {
      directory: path.join(BASE_PATH, "migrations")
    },
    seeds: {
      directory: path.join(BASE_PATH, "seeds")
    }
  },

  development: {
    client: "sqlite3",
    connection: {
      filename: path.join(__dirname, "data", "data.sqlite3")
    },
    migrations: {
      directory: path.join(BASE_PATH, "migrations")
    },
    seeds: {
      directory: path.join(BASE_PATH, "seeds")
    },
    useNullAsDefault: true
  },

  test: {
    client: "sqlite3",
    connection: {
      filename: ":memory:"
    },
    migrations: {
      directory: path.join(BASE_PATH, "migrations")
    },
    seeds: {
      directory: path.join(BASE_PATH, "seeds")
    },
    useNullAsDefault: true
  }
};
