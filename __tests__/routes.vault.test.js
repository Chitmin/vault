const request = require("supertest");
const app = require("../src/app.js");
const knex = require("../src/db/connection.js");

beforeEach(() => {
  return knex.migrate
    .rollback()
    .then(() => {
      return knex.migrate.latest();
    })
    .then(() => {
      return knex.seed.run();
    });
});

afterEach(() => {
  return knex.migrate.rollback();
});

afterAll(async done => {
  await knex.destroy();
  done();
});

test("Create a new item", async () => {
  const response = await request(app.callback())
    .post("/object")
    .send({ myKey: "value1" })
    .set("Accept", "application/json");

  expect(response.status).toBe(201);
  expect(response.body).toHaveProperty("key", "myKey");
  expect(response.body).toHaveProperty("value", "value1");
  expect(response.body).toHaveProperty("timestamp");
  expect(response.body).not.toHaveProperty("id");
});

test("Create 2 items with same key", async () => {
  const server = request(app.callback());
  const itemCreate = async item => {
    return await server
      .post("/object")
      .send(item)
      .set("Accept", "application/json");
  };

  const first = await itemCreate({ myKey: "value1" });

  // wait 0.1 second
  await new Promise(resolve => setTimeout(resolve, 100));

  const last = await itemCreate({ myKey: "value2" });

  expect(first.status).toBe(201);
  expect(last.status).toBe(201);
  expect(last.body.timestamp).toBeGreaterThan(first.body.timestamp);
});

test("Get an item by key", async () => {
  const server = request(app.callback());

  // insert an item first
  await server.post("/object").send({ newKey100: "newValue100" });

  const response = await server.get("/object/newKey100");

  expect(response.status).toBe(200);
  expect(response.body).toHaveProperty("key", "newKey100");
  expect(response.body).toHaveProperty("value", "newValue100");
  expect(response.body).toHaveProperty("timestamp");
  expect(response.body).not.toHaveProperty("id");
});

test("Get latest value of same key", async () => {
  const server = request(app.callback());

  // insert items first
  await server.post("/object").send({ newKey: "newValue1" });
  await server.post("/object").send({ newKey: "newValue2" });

  const response = await server.get("/object/newKey");

  expect(response.status).toBe(200);
  expect(response.body).toHaveProperty("key", "newKey");
  expect(response.body).toHaveProperty("value", "newValue2");
  expect(response.body).toHaveProperty("timestamp");
  expect(response.body).not.toHaveProperty("id");
});

test("Get an item by key & timestamp", async () => {
  const server = request(app.callback());

  // insert same key items with different timestamps
  const first = await server.post("/object").send({ newKey: "newValue1" });
  // just to ensure the timestamps
  await new Promise(resolve => setTimeout(resolve, 100));
  const last = await server.post("/object").send({ newKey: "newValue2" });

  const response = await server.get(
    "/object/newKey?timestamp=" + first.body.timestamp
  );

  expect(response.status).toBe(200);
  expect(response.body.timestamp).toBe(first.body.timestamp);
  expect(response.body.timestamp).toBeLessThan(last.body.timestamp);
});
