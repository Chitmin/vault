const Router = require("koa-router");
const items = require("../db/queries/items.js");
const router = new Router();

router.post("/object", async ctx => {
  try {
    const id = await items.save(setupData(ctx.request.body));
    const item = await items.find(id);
    if (item.hasOwnProperty("key")) {
      ctx.status = 201;
      ctx.body = item;
    } else {
      ctx.status = 400;
      ctx.body = {
        status: "error",
        message: "Something went wrong."
      };
    }
  } catch (err) {
    console.log(err);
  }
});

router.get("/object/:key", async ctx => {
  try {
    const item = await items.get(ctx.params.key, ctx.query.timestamp || null);
    if (item.hasOwnProperty("key")) {
      ctx.status = 200;
      ctx.body = item;
    } else {
      ctx.status = 404;
      ctx.body = {
        status: "error",
        message: "Item not found."
      };
    }
  } catch (err) {
    console.log(err);
  }
});

function setupData(body) {
  const key = Object.keys(body)[0];
  return { key: key, value: body[key] };
}

module.exports = router;
