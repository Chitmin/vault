const Koa = require("koa");
const bodyParser = require("koa-bodyparser");
const vaultRoutes = require("./routes/vault.js");

const app = new Koa();

app.use(bodyParser());
app.use(vaultRoutes.routes());

module.exports = app;
