exports.seed = knex => {
  const now = new Date().getTime();

  // Deletes ALL existing entries
  return knex("items")
    .del()
    .then(function () {
      // Inserts seed entries
      return knex("items").insert([
        { id: 1, key: "myKey", value: "value1", timestamp: now },
        { id: 2, key: "otherKey", value: "value1", timestamp: now },
        { id: 3, key: "myKey", value: "value2", timestamp: now + 86400 }
      ]);
    });
};
