exports.up = knex => {
  return knex.schema.createTable("items", table => {
    table.increments();
    table.string("key").notNullable();
    table.string("value").notNullable();
    table.bigInteger("timestamp").unsigned().notNullable();
  });
};

exports.down = knex => knex.schema.dropTable("items");
