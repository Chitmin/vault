const knex = require("../connection.js");

function save(item) {
  item.timestamp = new Date().getTime();
  return knex("items").insert(item);
}

function get(key, timestamp = null) {
  return knex("items")
    .first("key", "value", "timestamp")
    .where(builder => {
      builder.where("key", key);

      timestamp = parseInt(timestamp, 10);
      if (timestamp) {
        builder.andWhere("timestamp", "<=", timestamp);
      }
    })
    .orderBy("timestamp", "desc");
}

function find(id) {
  return knex("items")
    .first("key", "value", "timestamp")
    .where("id", parseInt(id, 10));
}

module.exports = {
  save,
  get,
  find
};
