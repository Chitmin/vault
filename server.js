const app = require("./src/app.js");
require("dotenv").config();

const port = process.env.PORT || 3000;
const server = app.listen(port);

module.exports = server;
