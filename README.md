## Requirements

- `node 12.16.x`
- `mysql`
- `sqlite3`

## To Run

- `npm install`
- `npm start`

## To Test

- `npm test`

## Endpoints

#### Save Item

**Require Header**: `"Content-Type: application/json"`

_Request_: `POST:` `/object` `{"myKey": "value1"}`
_Response_: `{"key": "myKey", "value": "value1", "timestamp": 12345600}`

_Request_: `POST:` `/object` `{"myKey": "value2"}`
_Response_: `{"key": "myKey", "value": "value2", "timestamp": 12345700}`

#### Get Item

_Request_: `Get:` `/object/myKey`
_Response_: `{"key": "myKey", "value": "value2", "timestamp": 12345700}`

_Request_: `Get:` `/object/myKey?timestamp=12345600`
_Response_: `{"key": "myKey", "value": "value1", "timestamp": 12345600}`
